# Гурток 2023-08-06

Створення та використання Dev Container у VS Code.

## План робіт

1. Створити репозиторій
    - `git start`
2. Додати в репозиторій `devcontainer.json`
    - `Dev Containers: Add Dev Container Configuration Files..`
3. Відкрити папку через Dev Container
    - `Dev Containers: Reopne in Container`
4. Змінити налаштування (доставити якісь залежності) та перевідкрити папку
 - створили poetry проект (poetry init, config)
 - вставоли python в devcontainers
 - встовили scrapy (poetry add scrapy)
 - протягом цього всього встановлювали екстеншени в devcontainers (для toml і python)
 - після цього всього робимо rebuild
 - створили postCreate.sh - з командою poetry install (маємо пустий шаблон проекту)
5. Запушити репозиторій на GitHub
 - відкрили code/codespace
6. Відкрити репозиторій через Codespaces
 - Done
